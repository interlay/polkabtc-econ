from const import BTC_IN_SAT, DOT_IN_PLANCK


class Environment:
    def __init__(self):
        self.planck_tx_cost = 0 # use fixed tx cost for now
        self.op_return_segwit_tx_bytes = 265  # const
        self.op_return_tx_bytes = 374  # const
        self.sat_per_byte_fast = 0
        self.sat_per_byte_medium = 0
        self.sat_per_byte_slow = 0
        self.sat_in_planck = 0
        self.planck_in_sat = 0
        self.sat_in_usd = 0
        self.planck_in_usd = 0

    # get the latest rates for BTC and DOT
    def setCurrentExchangeRates(self, btc_in_usd, dot_in_usd):
        # TODO: use https://min-api.cryptocompare.com/
        self.planck_in_usd = dot_in_usd / DOT_IN_PLANCK
        self.sat_in_usd = btc_in_usd / BTC_IN_SAT
        self.sat_in_planck = self.sat_in_usd / self.planck_in_usd
        self.planck_in_sat = self.planck_in_usd / self.sat_in_usd

    def setTransactionCosts(self, sat_per_byte_fast, planck_tx_cost):
        self.sat_per_byte_fast = sat_per_byte_fast
        self.planck_tx_cost = planck_tx_cost

    def getBTCTransactionCosts(self, num_txs):
        return self.op_return_tx_bytes * self.sat_per_byte_fast * num_txs / BTC_IN_SAT
    
    def getDOTTransactionCosts(self, num_txs):
        return self.planck_tx_cost * num_txs / DOT_IN_PLANCK

    def btcToUsd(self, btc):
        return btc * BTC_IN_SAT * self.sat_in_usd
    
    def dotToUsd(self, dot):
        return dot * DOT_IN_PLANCK * self.planck_in_usd