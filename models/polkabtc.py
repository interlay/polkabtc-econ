from environment import Environment
from agents import Agents
from const import DAYS, BTC_IN_SAT, DOT_IN_PLANCK


class VaultRegistry:
    def __init__(self):
        self.min_collateral = 100
        self.punishment_fee = 0
        self.punishment_delay = 0
        self.redeem_premium_fee = 0
        self.secure_collateral_threshold = 2.0 # 200 %
        self.auction_collateral_threshold = 1.5  # 150%
        self.premium_redeem_threshold = 1.2  # 120%
        self.liquidation_threshold = 1.1  # 110%

    def setModel(self, min_collateral, punishment_fee, punishment_delay, redeem_premium_fee):
        self.min_collateral = min_collateral
        self.punishment_fee = punishment_fee
        self.punishment_delay = punishment_delay
        self.redeem_premium_fee = redeem_premium_fee

    def setThresholds(self, secure, auction, premium, liquidation):
        self.secure_collateral_threshold = secure
        self.auction_collateral_threshold = auction
        self.premium_redeem_threshold = premium
        self.liquidation_threshold = liquidation

class StakedRelayer:
    def __init__(self):
        self.vote_threshold = 1
        self.min_collateral = 100
    
    def setModel(self, vote_threshold, min_collateral):
        self.vote_threshold = vote_threshold
        self.min_collateral = min_collateral

class Issue:
    def __init__(self):
        self.griefing_collateral = 100
        self.period = DAYS
        self.fee = 0.0
    
    def setModel(self, griefing_collateral, fee):
        self.griefing_collateral = griefing_collateral
        self.fee = fee


class Redeem:
    def __init__(self):
        self.period = DAYS
        self.fee = 0
    
    def setModel(self, fee):
        self.fee = fee


class Replace:
    def __init__(self):
        self.griefing_collateral = 100
        self.period = DAYS

    def setModel(self, griefing_collateral):
        self.griefing_collateral = griefing_collateral

class Oracle:
    def __init__(self):
        self.sat_per_byte_fast = 0
        self.sat_per_byte_medium = 0
        self.sat_per_byte_slow = 0
        self.sat_in_planck = 0.0
        self.planck_in_sat = 0.0

    def btc_to_dot(self, btc):
        planck = self.sat_in_planck * btc * BTC_IN_SAT
        dot = planck / DOT_IN_PLANCK
        return dot

    def dot_to_btc(self, dot):
        sat = self.planck_in_sat * dot * DOT_IN_PLANCK
        btc = sat / BTC_IN_SAT
        return btc

    def setRates(self, env: Environment):
        self.sat_per_byte_fast = env.sat_per_byte_fast
        self.sat_per_byte_medium = env.sat_per_byte_medium
        self.sat_per_byte_slow = env.sat_per_byte_slow
        self.sat_in_planck = env.sat_in_planck
        self.planck_in_sat = env.planck_in_sat

class FeeShare:
    def __init__(self):
        # distribution of fee shares
        self.vault_issue_share = 0.8
        self.relayer_issue_share = 0.05
        self.maintainer_issue_share = 0.15
        self.collator_issue_share = 0.0 
        self.vault_redeem_share = 0.8
        self.relayer_redeem_share = 0.05
        self.maintainer_redeem_share = 0.15
        self.collator_redeem_share = 0.0 
    
    def setIssue(self, vault, relayer, maintainer, collator):
        assert(vault + relayer + maintainer + collator >= 1.0, "Must add up to 1.0 = 100%")
        self.vault_issue_share = vault
        self.relayer_issue_share = relayer
        self.maintainer_issue_share = maintainer
        self.collator_redeem_share = collator 

    def setRedeem(self, vault, relayer, maintainer, collator):
        assert(vault + relayer + maintainer + collator >= 1.0, "Must add up to 1.0 = 100%")
        self.vault_redeem_share = vault
        self.relayer_redeem_share = relayer
        self.maintainer_redeem_share = maintainer
        self.collator_redeem_share = collator 

class Subsidy:
    def __init__(self):
        self.vault_share = 0.8
        self.relayer_share = 0.05
        self.maintainer_share = 0.15
        self.collator_share = 0.00

    def setShare(self, vault, relayer, maintainer, collator):
        assert(vault + relayer + maintainer + collator >= 1.0, "Must add up to 1.0 = 100%")
        self.vault_share = vault
        self.relayer_share = relayer
        self.maintainer_share = maintainer
        self.collator_share = collator 

class PolkaBTC:
    def __init__(self):
        self.total_polkaBTC = 0.0
        self.total_locked_DOT = 0.0
        self.vault_registry = VaultRegistry()
        self.staked_relayer = StakedRelayer()
        self.issue = Issue()
        self.redeem = Redeem()
        self.replace = Replace()
        self.oracle = Oracle()
        self.fee_share = FeeShare()
        self.subsidy = Subsidy()

    # Update the balances and fees for each agent based on the system parameters
    def execute_issue(self, env: Environment, agents: Agents, new_btc, btc_per_tx):
        # set global values
        self.total_polkaBTC += new_btc
        new_locked_dot = self.oracle.btc_to_dot(new_btc) * self.vault_registry.secure_collateral_threshold
        self.total_locked_DOT += new_locked_dot
        # calculate transaction fees
        # issue process requires 1 BTC tx and 2 DOT txs
        num_btc_txs = new_btc / btc_per_tx
        num_dot_txs = 2 * num_btc_txs
        issue_btc_tx_fees = env.getBTCTransactionCosts(num_btc_txs)
        issue_dot_tx_fees = env.getDOTTransactionCosts(num_dot_txs)

        # calculate issue fees
        total_issue_fees_in_polkabtc = new_btc * self.issue.fee
        user_issue_fee = total_issue_fees_in_polkabtc
        vault_issue_fee = total_issue_fees_in_polkabtc * self.fee_share.vault_issue_share
        relayer_issue_fee = total_issue_fees_in_polkabtc * self.fee_share.relayer_issue_share
        maintainer_issue_fee = total_issue_fees_in_polkabtc * self.fee_share.maintainer_issue_share
        collator_issue_fee = total_issue_fees_in_polkabtc * self.fee_share.collator_issue_share
        
        # update agents
        agents.user.issue(new_btc, user_issue_fee, issue_btc_tx_fees, issue_dot_tx_fees)
        agents.vault.issue(new_locked_dot, new_btc, vault_issue_fee)
        agents.relayer.issue(relayer_issue_fee)
        agents.maintainer.issue(maintainer_issue_fee)
        agents.collator.issue(collator_issue_fee, issue_dot_tx_fees)

    # pay an extra subsidy to participating agents
    def pay_subsidy(self, agents: Agents, dot_amount):
        # TODO: move this to functions inside the agents
        agents.vault.fees_DOT += dot_amount * self.subsidy.vault_share
        agents.relayer.fees_DOT += dot_amount * self.subsidy.relayer_share
        agents.maintainer.fees_DOT += dot_amount * self.subsidy.maintainer_share
        agents.collator.fees_DOT += dot_amount * self.subsidy.collator_share