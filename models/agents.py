from environment import Environment
import pandas as pd


class BasicAgent:
    def __init__(self):
        self.balance_DOT = 0.0
        self.balance_BTC = 0.0
        # all fees positive if receiving fees, negative if paying fees
        self.fees_DOT = 0.0
        self.fees_PolkaBTC = 0.0
        self.tx_fee_DOT = 0.0


class Vault(BasicAgent):
    def __init__(self):
        super().__init__()
        self.locked_DOT = 0.0
        self.locked_BTC = 0.0
        self.tx_fee_BTC = 0.0

    def issue(self, dot, btc, issue_polkabtc_fees):
        self.locked_DOT += dot
        self.locked_BTC += btc
        self.fees_PolkaBTC += issue_polkabtc_fees


class StakedRelayer(BasicAgent):
    def __init__(self):
        super().__init__()
        self.locked_DOT = 1_000_000.0  # some default value

    def issue(self, issue_polkabtc_fees):
        self.fees_PolkaBTC += issue_polkabtc_fees


class User(BasicAgent):
    def __init__(self):
        super().__init__()
        self.balance_PolkaBTC = 0.0
        self.fees_PolkaBTC = 0.0
        self.tx_fee_BTC = 0.0

    def issue(self, polkabtc, polkabtc_fees, btc_tx_fees, dot_tx_fees):
        self.balance_PolkaBTC += (polkabtc - polkabtc_fees)
        self.fees_PolkaBTC -= polkabtc_fees
        self.tx_fee_BTC -= btc_tx_fees
        self.tx_fee_DOT -= dot_tx_fees


class Maintainer(BasicAgent):
    def __init__(self):
        super().__init__()

    def issue(self, issue_polkabtc_fees):
        self.fees_PolkaBTC += issue_polkabtc_fees


class Collator(BasicAgent):
    def __init__(self):
        super().__init__()
        self.locked_DOT = 10_000.0  # some default value

    def issue(self, issue_polkabtc_fees, dot_tx_fees):
        self.fees_PolkaBTC += issue_polkabtc_fees
        self.tx_fee_DOT += dot_tx_fees


class Agents:
    def __init__(self):
        self.vault = Vault()
        self.relayer = StakedRelayer()
        self.user = User()
        self.maintainer = Maintainer()
        self.collator = Collator()

    # this function returns the total fees paid/received in USD at the
    # current exchange rate. If the agent collected fees, the return value is positive,
    # otherwise th return value is negative.
    #
    # returns a pandas DataFrame with fixed position elements by agent
    # ordering: user, vault, relayer, maintainer, collator
    def get_accumulated_fees_in_usd(self, env: Environment, total_btc):
        # calculate absolute fees
        user_fees = env.btcToUsd(self.user.fees_PolkaBTC + self.user.tx_fee_BTC) + \
            env.dotToUsd(self.user.fees_DOT + self.user.tx_fee_DOT)
        vault_fees = env.btcToUsd(self.vault.fees_PolkaBTC + self.vault.tx_fee_BTC) + \
            env.dotToUsd(self.vault.fees_DOT + self.vault.tx_fee_DOT)
        relayer_fees = env.btcToUsd(self.relayer.fees_PolkaBTC) + \
            env.dotToUsd(self.relayer.fees_DOT + self.relayer.tx_fee_DOT)
        maintainer_fees = env.btcToUsd(
            self.maintainer.fees_PolkaBTC) + env.dotToUsd(self.maintainer.fees_DOT)
        collator_fees = env.btcToUsd(self.collator.fees_PolkaBTC) + \
            env.dotToUsd(self.collator.fees_DOT + self.collator.tx_fee_DOT)

        # calculate apy based on locked dot of each agent
        user_apy = 0.0
        vault_apy = 0.0 if self.vault.locked_DOT == 0 else vault_fees / env.dotToUsd(self.vault.locked_DOT)
        relayer_apy = 0.0 if self.relayer.locked_DOT == 0 else relayer_fees / env.dotToUsd(self.relayer.locked_DOT)
        maintainer_apy = 0.0  # no DOT locked
        collator_apy = 0.0 if self.collator.locked_DOT == 0 else collator_fees / env.dotToUsd(self.collator.locked_DOT)

        columns = ["total_btc", "locked_dot", "locked_dot_usd", "agents", "fees_abs_usd", "fees_apy_percent"]
        agents = ["user", "vault", "relayer", "maintainer", "collator"]
        locked_dot = [0.0, self.vault.locked_DOT,
                      self.relayer.locked_DOT, 0.0, self.collator.locked_DOT]
        locked_dot_usd = [env.dotToUsd(x) for x in locked_dot]
        fees_abs = [user_fees, vault_fees, relayer_fees,
                    maintainer_fees, collator_fees]
        fees_apy = [user_apy, vault_apy, relayer_apy,
                    maintainer_apy, collator_apy]
        # convert into percentages
        fees_apy = [x * 100 for x in fees_apy]

        data = {
            "total_btc": [total_btc] * len(agents),
            "agents": agents,
            "locked_dot": locked_dot,
            "locked_dot_usd": locked_dot_usd,
            "fees_abs_usd": fees_abs,
            "fees_apy_percent": fees_apy
        }

        return pd.DataFrame(data, columns=columns)

    def reset(self):
        self.__init__()
