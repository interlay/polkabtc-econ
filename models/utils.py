from const import BTC_IN_SAT, DOT_IN_PLANCK 

def satToBtc(sat):
    return sat / BTC_IN_SAT

def btcToSat(btc):
    return btc * BTC_IN_SAT

def planckToDot(planck):
    return planck / DOT_IN_PLANCK

def dotToPlanck(dot):
    return dot * DOT_IN_PLANCK