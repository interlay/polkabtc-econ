# polkabtc-sim

A repository dedicated to analyze incentives in the [BTC-Parachain](https://gitlab.com/interlay/btc-parachain).

## About

PolkaBTC is a system bridging Bitcoin to the Polkadot ecosystem.

## Getting Started

### Prerequisites

Install pandoc: https://pandoc.org/installing.html

### Installation

Clone the repository.

```bash
git clone git@github.com:interlay/polkabtc-sim.git
```

Setup a virtual environment.

```bash
python -m venv venv
source venv/bin/activate
```

Install the required dependencies including [JupyterLab](https://jupyter.org/install).

```bash
pip install -r requirements.txt
```

## Usage

Start Jupyter Lab and open the relevant notebooks.

```bash
jupyter-lab
```

## Docs

The docs are build using Sphinx. If you want to update the docs, you can edit and host the documentation locally.

```bash
sphinx-autobuild ./ _build
```

## Release

To update the spec on GitHub pages, you need to manually update the static build files.

```bash
make github
```

## License

(C) Copyright 2021 [Interlay](https://www.interlay.io) Ltd

polkabtc-sim is licensed under the terms of the Apache License (Version 2.0). See [LICENSE](LICENSE).

## Contact

Website: [Interlay.io](https://www.interlay.io)

Twitter: [@interlayHQ](https://twitter.com/InterlayHQ)

Email: contact@interlay.io

## Acknowledgements

We would like to thank the following teams for their continuous support:

* [Web3 Foundation](https://web3.foundation/)
* [Parity Technologies](https://www.parity.io/)
