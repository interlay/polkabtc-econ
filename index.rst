.. polkabtc-sim documentation master file

Welcome to the PolkaBTC economics documentation!
================================================

This documentation provides the economic models in PolkaBTC and describes the incentives for the involved parties.
Feel free to browse the web version below or read it as a `PDF <https://interlay.gitlab.io/polkabtc-econ/polkabtc-fee-model.pdf>`_.

The overall description of the the economic models are found in the `specification <https://interlay.gitlab.io/polkabtc-spec/economics/incentives.html>`_.

.. toctree::
   :maxdepth: 1
   :caption: Models

   models/basic
   models/basic_with_subsidy
   models/staked_relayer_SLA_fees